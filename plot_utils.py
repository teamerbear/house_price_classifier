import matplotlib.pyplot as plt
import numpy as np

def plot_continuous_distribution(col_df, file_name):
  """
  Plots the distribution of a continuous variable with the given name. The plot
  is created using kernel density estimation (KDE) based on the given data
  samples.

  Arguments:
    col_df:   The pandas DataFrame object containing a single column of data to
              plot. This data represents samples drawn from a continuous
              (i.e. non-categorical) distribution.
    file_name:  The name of a file where the plot will be saved.
  """
  # TODO: Make this look nicer.
  plt.clf()
  col_name = list(col_df.columns)[0]
  ax = col_df.plot.kde() # Use default KDE params for now.
  plt.xlim(col_df.min()[0], col_df.max()[0])
  plt.xlabel(col_name)
  plt.ylabel("Density")
  plt.title(col_name)
  fig = ax.get_figure()
  fig.savefig(file_name)

def plot_categorical_distribution(col_series, col_name, file_name):
  """
  Produces a bar plot of the given categorical data.

  Arguments:
    col_df:     The pandas DataFrame object containing a single column of data
                to plot. This data represents samples drawn from a multinomial
                (i.e. categorical) distribution.
    file_name:  The name of a file where the plot will be saved.
  """
  # TODO: Make this look nicer.
  plt.clf()
  ax = col_series.value_counts().plot.bar(x=col_name)
  plt.xlabel(col_name)
  plt.ylabel("Occurences")
  plt.title(col_name)
  fig = ax.get_figure()
  fig.savefig(file_name)
