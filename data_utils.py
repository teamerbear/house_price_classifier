from copy import deepcopy
import numpy as np
import pandas as pd
from sklearn.feature_selection import chi2
import sys

def get_basic_stats(df):
  """
  Gets the sample mean and variance for each column of data in the DataFrame
  passed in.

  Arguments:
    df:   The pandas DataFrame object containing the data.
  
  Returns:
    mean_dict:  A dictionary containing the sample mean for each column of data.
                (key: column name, value: corresponding sample mean)
    vari_dict:  A dictionary containing the sample variance for each column of
                data.  (key: column name, value: corresponding sample variance)
  """
  col_list = list(df.columns)
  mean_dict = {}
  vari_dict = {}

  # Compute the mean and variance for all columns.
  mean_df = df.mean(axis=0, skipna=True)
  vari_df = df.var(axis=0, skipna=True)

  # Pack the means and variances into a dictionary.
  for i in range(len(col_list)):
    mean_dict[col_list[i]] = mean_df[i]
    vari_dict[col_list[i]] = vari_df[i]
  
  return [mean_dict, vari_dict]

def get_cleaned_data(input_df, target_var, test=False):
  """
  Gets the data into a format where we can start exploring relationships, feed
  into an ML model, etc. Changes data types from 'object' to either 'category'
  for categorical data or 'float64' for continuous data.

  Arguments:
    input_df:     The data that we want to clean in the pandas DataFrame format.
    target_var:   The column name to use as the target variable.
  
  Returns:
    cleaned_cat_features:   The output (cleaned) feature data for categorical
                            features in a pandas DataFrame.
    cleaned_numeric_cats:   The cleaned categorical data in numeric form.
    cleaned_cont_features:  The output (cleaned) feature data for continuous
                            variables in a pandas DataFrame.
    cleaned_cat_labels:     The output (cleaned) continuous label data grouped
                            into two categories (> 200k, <= 200k)
    cleaned_cont_labels:    The output (cleaned) continuous label data in a
                            pandas DataFrame.
  """
  cleaned_features = deepcopy(input_df)
  cleaned_labels = None
  if not test:
    cleaned_labels = deepcopy(cleaned_features[[target_var]]) 
    cleaned_features = cleaned_features.drop(target_var, axis=1)

  all_cols = cleaned_features.columns
  continuous_vars = [ 'LotFrontage', 'LotArea', 'YearBuilt',
    'YearRemodAdd', 'MasVnrArea', 'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF',
    'TotalBsmtSF', '1stFlrSF', '2ndFlrSF', 'LowQualFinSF', 'GrLivArea',
    'BsmtFullBath', 'BsmtHalfBath', 'FullBath', 'HalfBath', 'BedroomAbvGr',
    'KitchenAbvGr', 'TotRmsAbvGrd', 'Fireplaces', 'GarageYrBlt', 'GarageCars',
    'GarageArea', 'WoodDeckSF', 'OpenPorchSF', 'EnclosedPorch', '3SsnPorch',
    'ScreenPorch', 'PoolArea', 'MiscVal', 'YrSold' ]

  categorical_vars = []
  for col_name in all_cols:
    found = False
    for cont_name in continuous_vars:
      if col_name == cont_name:
        found = True
    if not found:
      categorical_vars.append(col_name)

  # Pandas doesn't support changing dtype to category for more than one column
  # at a time, so we'll loop through them.
  for var_name in categorical_vars:
    cleaned_features[var_name] = \
      cleaned_features[var_name].astype('category')
  cleaned_numeric_cats = deepcopy(cleaned_features[categorical_vars])

  for var_name in categorical_vars:
    cleaned_numeric_cats[var_name] = cleaned_numeric_cats[var_name].cat.codes
    
    # The chi-squared test won't work with negative values (NAs get mapped to
    # -1), so we just make this a separate category.
    change_it = False
    for element in cleaned_numeric_cats[var_name]:
      if element < 0:
        change_it = True
        break
    
    if change_it:
      max_cat = cleaned_numeric_cats[var_name].max()
      cleaned_numeric_cats[var_name] = \
        cleaned_numeric_cats[var_name].replace(-1, max_cat + 1)
  
  # TODO: If this doesn't work, could fill NAs with the average instead of zero
  #       or with the mode maybe?
  cleaned_features[continuous_vars] = \
    cleaned_features[continuous_vars].fillna('0').astype('float64')
  cleaned_cont_labels = None
  cleaned_cat_labels = None
  if not test:
    cleaned_cont_labels = cleaned_labels.astype('float64')
    cleaned_cat_labels = deepcopy(cleaned_cont_labels)
    max_label = cleaned_cat_labels[target_var].max()
    cleaned_cat_labels[target_var] = \
      pd.cut(cleaned_cat_labels[target_var], [0.0, 200000.0, (2 * max_label)], labels=['1', '2'])
    cleaned_cat_labels = cleaned_cat_labels.astype('category')

  return [cleaned_features[categorical_vars], \
          cleaned_numeric_cats, \
          cleaned_features[continuous_vars], \
          cleaned_cat_labels, cleaned_cont_labels]

def perform_chi_squared(features_df, labels_df):
  """
  Performs a chi-squared test to see if the given categorical variables are
  independent of the target variable.

  Arguments:
    features_df:  The pandas DataFrame object with categorical variable
                  features.
    labels_df:    The pandas DataFrame object with the labels.

  Return:
    p_val_dict:   A dictionary of p-values for each categorical variable
                  (key: variable name, value: corresponding p-value).
  """
  [_, p_vals] = chi2(features_df.to_numpy(), labels_df.to_numpy())

  p_val_dict = {}
  col_list = list(features_df.columns)
  for i in range(len(col_list)):
    p_val_dict[col_list[i]] = p_vals[i]

  return p_val_dict
  
def perform_correlation(features_df, labels_df, label_name):
  """
  Performs a correlation on each of the feature columns (continous) with the
  target variable column.

  Arguments:
    features_df:  The pandas DataFrame object with continuous variable
                  features.
    labels_df:    The pandas DataFrame object with the labels.
    label_name:   The column name of the labels.

  Return:
    corr_dict:   A dictionary of correlation values for each continuous
                  variable.
                    (key: variable name, value: corresponding correlation value)
  """
  corr_dict = {}

  # Concatenate label column with the features.
  num_cols = len(list(features_df.columns))
  corr_df = deepcopy(features_df)
  corr_df.insert(num_cols, label_name, labels_df[label_name])

  # Perform the correlation
  corr_matrix = corr_df.corr()

  # The last column contains the correlation of each column with the target
  # variable. We are interested in either large positive or large negative
  # correlation values to use as input to the model. Only keep variables where
  # the absolute value of the correlation to the target variable is above some
  # threshold.
  corrs = corr_matrix[label_name]
  col_names = list(corr_matrix.columns)
  for i in range(len(corrs)):
    # Skip the correlation of the target variable with itself.
    if label_name != col_names[i]:
      corr_dict[col_names[i]] = corrs[i]

  return corr_dict
  