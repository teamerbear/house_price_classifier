#!/usr/bin/env python3
from copy import deepcopy
import sys
import os, os.path
import csv
import pandas
import argparse
import math
import numpy as np
import shutil
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC

import data_utils
import plot_utils

# Read command line args
parser = argparse.ArgumentParser(description='AMES Housing Data Modeling')
parser.add_argument('-d','--data',help='Training data', required=True)
parser.add_argument('-t','--test',help='Testing data', required=True)
parser.add_argument('-v', '--verbose',
                    help='Verbose mode for debugging',
                    required=False,
                    action='store_true')

args = parser.parse_args()
if(args.verbose):
    sys.stderr.write("DEBUG: training data: %s\n" % (args.data))
    sys.stderr.write("DEBUG: test data: %s\n" % (args.train))

col_names = [
  'MSSubClass','MSZoning','LotFrontage','LotArea','Street','Alley',
  'LotShape','LandContour','Utilities','LotConfig','LandSlope','Neighborhood',
  'Condition1','Condition2','BldgType','HouseStyle','OverallQual','OverallCond',
  'YearBuilt','YearRemodAdd','RoofStyle','RoofMatl','Exterior1st','Exterior2nd',
  'MasVnrType','MasVnrArea','ExterQual','ExterCond','Foundation','BsmtQual',
  'BsmtCond','BsmtExposure','BsmtFinType1','BsmtFinSF1','BsmtFinType2',
  'BsmtFinSF2','BsmtUnfSF','TotalBsmtSF','Heating','HeatingQC','CentralAir',
  'Electrical','1stFlrSF','2ndFlrSF','LowQualFinSF','GrLivArea','BsmtFullBath',
  'BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr','KitchenAbvGr',
  'KitchenQual','TotRmsAbvGrd','Functional','Fireplaces','FireplaceQu',
  'GarageType','GarageYrBlt','GarageFinish','GarageCars','GarageArea',
  'GarageQual','GarageCond','PavedDrive','WoodDeckSF','OpenPorchSF',
  'EnclosedPorch','3SsnPorch','ScreenPorch','PoolArea','PoolQC','Fence',
  'MiscFeature','MiscVal','MoSold','YrSold','SaleType','SaleCondition',
  'SalePrice'
]

# Read data  (CSV of tabs seperated columns) - make sure to clean data as needed
target_var = 'SalePrice'
test_feature_names = deepcopy(col_names)
test_feature_names.remove(target_var)
df_data = pandas.read_csv(
  args.data, comment='#', sep=',', names=col_names, header=0, dtype=str)
df_test = pandas.read_csv(
  args.test, comment='#', sep=',', names=test_feature_names, header=0, dtype=str)

print ( df_data.shape )
print ( df_test.shape )


# Split the data set into a training set and a validation set.
#   Note: use a random seed so that this is reproducible.
train_features, val_features, train_labels, val_labels = train_test_split(
  df_data[test_feature_names], df_data[target_var],
  test_size=0.25, random_state=13)

train_data = deepcopy(train_features)
train_data.insert(len(col_names) - 1, target_var, train_labels)
val_data = deepcopy(val_features)
val_data.insert(len(col_names) - 1, target_var, val_labels)

# First, get the data into a format we can use.
[train_cat_features, train_num_cat_features, \
  train_cont_features, train_cat_labels, train_cont_labels] \
  = data_utils.get_cleaned_data(train_data, target_var)

[_, val_num_cat_features, val_cont_features,
  val_cat_labels, val_cont_labels] \
  = data_utils.get_cleaned_data(val_data, target_var)

[full_train_cat_features, full_train_num_cat_features, \
  full_train_cont_features, full_train_cat_labels, full_train_cont_labels] \
  = data_utils.get_cleaned_data(df_data, target_var)

[full_test_cat_features, full_test_num_cat_features, \
  full_test_cont_features, full_test_cat_labels, full_test_cont_labels] \
  = data_utils.get_cleaned_data(df_test, target_var, test=True)

# Calculate sample mean and variance of all variables in both data sets (train
# and test) for future reference.
[cat_mean_dict, cat_vari_dict] = \
  data_utils.get_basic_stats(train_num_cat_features)
[cont_mean_dict, cont_vari_dict] = \
  data_utils.get_basic_stats(train_cont_features)
[lab_mean_dict, lab_vari_dict] = \
  data_utils.get_basic_stats(train_cont_labels)

[cat_val_mean_dict, cat_val_vari_dict] = \
  data_utils.get_basic_stats(val_num_cat_features)
[cont_val_mean_dict, cont_val_vari_dict] = \
  data_utils.get_basic_stats(val_cont_features)
[lab_val_mean_dict, lab_val_vari_dict] = \
  data_utils.get_basic_stats(val_cont_labels)

[full_train_cat_mean_dict, full_train_cat_vari_dict] = \
  data_utils.get_basic_stats(full_train_num_cat_features)
[full_train_cont_mean_dict, full_train_cont_vari_dict] = \
  data_utils.get_basic_stats(full_train_cont_features)
[full_train_lab_mean_dict, full_train_lab_vari_dict] = \
  data_utils.get_basic_stats(full_train_cont_labels)

[full_test_cat_mean_dict, full_test_cat_vari_dict] = \
  data_utils.get_basic_stats(full_test_num_cat_features)
[full_test_cont_mean_dict, full_test_cont_vari_dict] = \
  data_utils.get_basic_stats(full_test_cont_features)

# Write the means and variances to a file.
with open('basic_stats_train.txt', 'w') as stat_file:
  stat_file.write("Categorical Variables:\n")
  for key in full_train_cat_mean_dict:
    stat_file.write("\t{}:  Mean: {}, Variance: {}\n".format(
      key, full_train_cat_mean_dict[key], full_train_cat_vari_dict[key]))

  stat_file.write("\nContinuous Variables:\n")
  for key in full_train_cont_mean_dict:
    stat_file.write("\t{}:  Mean: {}, Variance: {}\n".format(
      key, full_train_cont_mean_dict[key], full_train_cont_vari_dict[key]))
  
  stat_file.write("\nTarget Variable:\n")
  lab_name = target_var
  stat_file.write("\t{}:  Mean: {}, Variance: {}\n".format(
    lab_name, full_train_lab_mean_dict[lab_name],
    full_train_lab_vari_dict[lab_name]))

with open('basic_stats_test.txt', 'w') as stat_file:
  stat_file.write("Categorical Variables:\n")
  for key in full_test_cat_mean_dict:
    stat_file.write("\t{}:  Mean: {}, Variance: {}\n".format(
      key, full_test_cat_mean_dict[key], full_test_cat_vari_dict[key]))

  stat_file.write("\nContinuous Variables:\n")
  for key in full_test_cont_mean_dict:
    stat_file.write("\t{}:  Mean: {}, Variance: {}\n".format(
      key, full_test_cont_mean_dict[key], full_test_cont_vari_dict[key]))
  
# Perform a chi-squared test on the categorical variables to see if the target
# variable is dependent on them.
cat_p_vals = data_utils.perform_chi_squared(
  train_num_cat_features, train_cat_labels)

ignored_vars = []
used_vars = []

shutil.rmtree("plots")
if not os.path.isdir(os.path.join("plots", "categorical")):
  if not os.path.isdir("plots"):
    os.mkdir("plots")
  os.mkdir(os.path.join("plots", "categorical"))

# The null hypothesis for the chi-squared test is that the target variable 
# (label) is independent of the particular feature. Explore any variable where
# the probability of independence is less than 10% for now. If the probability
# of independence is greater than 10%, we won't use this feature in our model.
print("\nPlotting categorical distributions . . .")
chi_2_p_thresh = 0.1
total_cat = len(cat_p_vals)
used_cat = []
for key in cat_p_vals:
  if cat_p_vals[key] <= chi_2_p_thresh:
    plot_utils.plot_categorical_distribution(
      train_cat_features[key], key,
      os.path.join("plots", "categorical", "{}_dist.png".format(key)))
    used_vars.append(key)
    used_cat.append(key)
  else:
    ignored_vars.append(key)

plot_utils.plot_categorical_distribution(
  train_cat_labels[target_var], target_var,
  os.path.join("plots", "categorical", "{}_dist.png".format(target_var)))

print("---- Using {} out of {} categorical variables".format(
  len(used_cat), total_cat))

print("\nPlotting continuous distributions . . .")
if not os.path.isdir(os.path.join("plots", "continuous")):
  os.mkdir(os.path.join("plots", "continuous"))
plot_utils.plot_continuous_distribution(
  train_cont_labels,
  os.path.join("plots", "continuous", "{}_dist.png".format(target_var)))

# Perform a correlation of all the continuous variables with the target variable
# to see if there is a possible relationship between them.
corr_dict = data_utils.perform_correlation(
  train_cont_features, train_cont_labels, target_var)
corr_thresh = 0.5

total_cont = len(corr_dict)
used_cont = []
for key in corr_dict:
  if abs(corr_dict[key]) >= corr_thresh:
    plot_utils.plot_continuous_distribution(
      train_cont_features[[key]],
      os.path.join("plots", "continuous", "{}_dist.png".format(key)))
    used_vars.append(key)
    used_cont.append(key)
  else:
    ignored_vars.append(key)

print("---- Using {} out of {} continuous variables".format(
  len(used_cont), total_cont))

# Write out the variables we'll use to a file for future reference.
with open('used_vars.txt', 'w') as out_file:
  out_file.write("Categorical\n")
  for var in used_cat:
    out_file.write("\t{}\n".format(var))

  out_file.write("Continuous\n")
  for var in used_cont:
    out_file.write("\t{}\n".format(var))

# Write out the variables we aren't using to a file for future reference.
with open('ignored_vars.txt', 'w') as out_file:
  for var in ignored_vars:
    out_file.write("{}\n".format(var))

# Put the data from all the variables we care about into a single frame to feed
# into the model.
final_train_features = pandas.DataFrame()
final_test_features = pandas.DataFrame()
insert_ind = 0
for col_name in used_cat:
  final_train_features.insert(
    insert_ind, col_name, train_num_cat_features[col_name])
  final_test_features.insert(
    insert_ind, col_name, val_num_cat_features[col_name])
  insert_ind += 1

for col_name in used_cont:
  final_train_features.insert(
    insert_ind, col_name, train_cont_features[col_name])
  final_test_features.insert(
    insert_ind, col_name, val_cont_features[col_name])
  insert_ind += 1

# Explore learning rates and number of estimators.
print("\nHyperparameter Exploration:")
lr_arr = [ x / 4.0 for x in range(4, 9)]
num_estimators = [ x * 10 for x in range(1, 11)]
train_labels = np.squeeze(train_cat_labels.to_numpy())
best_score = 0.0
best_lr = 0.0
best_num_est = 0
best_model = None
for lr in lr_arr:
  for num_est in num_estimators:
    # Train the classifier with the given hyperparams.
    # TODO: This could be threaded for quicker training, but the training isn't
    #       very slow on this small dataset anyway.
    clf = AdaBoostClassifier(
      learning_rate=lr, n_estimators=num_est, random_state=13)
    clf.fit(final_train_features.to_numpy(), train_labels)

    # Predict the labels of the training set with the trained classifier.
    train_acc = clf.score(final_train_features.to_numpy(), train_labels)
    pred_labels = clf.predict(final_test_features)
    test_acc = accuracy_score(val_cat_labels, pred_labels)

    # Keep track of the best score and hyperparams.
    if test_acc > best_score:
      best_score = test_acc
      best_lr = lr
      best_num_est = num_est
      best_model = clf

print("\nBest Test Set Accuracy: {}".format(best_score))
print("\tLearning Rate: {}".format(best_lr))
print("\tNum Estimators: {}\n".format(best_num_est))

#### UNCOMMENT FOR SVM
#print("\nHyperparameter Exploration:")
#penalties = [ 'l1', 'l2' ]
#losses = [ 'hinge', 'squared_hinge' ]
#regs = [ x / 4.0 for x in range(8, 17) ]
#tols = [ 1e-1, 1e-2, 1e-3, 1e-4 ]
#train_labels = np.squeeze(train_cat_labels.to_numpy())
#best_score = 0.0
#best_pen = None
#best_loss = None
#best_reg = None
#best_tol = None
#best_model = None
#for penalty in penalties:
#  for loss in losses:
#    for reg in regs:
#      for tolerance in tols:
#        # Train the classifier with the given hyperparams.
#        # TODO: This could be threaded for quicker training, but the training
#        #       isn't very slow on this small dataset anyway.
#        clf = LinearSVC(
#          penalty=penalty, loss=loss, C=reg, tol=tolerance, max_iter=10000,
#          dual=False)
#        try:
#          clf.fit(final_train_features.to_numpy(), train_labels)
#
#          # Predict the labels of the training set with the trained classifier.
#          train_acc = clf.score(final_train_features.to_numpy(), train_labels)
#          pred_labels = clf.predict(final_test_features)
#          test_acc = accuracy_score(test_cat_labels, pred_labels)
#
#          # Keep track of the best score and hyperparams.
#          if test_acc > best_score:
#            print("Setting Best Accuracy to {}".format(test_acc))
#            best_score = test_acc
#            best_pen = penalty
#            best_loss = loss
#            best_reg = reg
#            best_tol = tolerance
#            best_model = clf
#        except ValueError:
#          # This will happen if there is an unsupported hyperparam combo.
#          pass
#
#print("\nBest Test Set Accuracy: {}".format(best_score))
#print("\tBest Penalty: {}".format(best_pen))
#print("\tBest Loss: {}".format(best_loss))
#print("\tBest C: {}".format(best_reg))
#print("\tBest Tolerance: {}".format(best_tol))